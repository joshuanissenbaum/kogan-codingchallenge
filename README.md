# Kogan Coding Challenge
Using the provided (paginated) API, find the average cubic weight for all products in the "Air Conditioners" category.

Cubic weight is calculated by multiplying the length, height and width of the parcel. The result is then multiplied by the industry standard cubic weight conversion factor of 250.

Language used: Javascript ES6

### Prerequisites
NodeJS - https://nodejs.org/en/
npm - https://www.npmjs.com/

## Running the project
In the project directory, you can run:

### `npm install`

Press 'Enter' to confirm Semantic UI setup - leaving each default option which is selected as the CLI prompts

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## Built With

* [React JS] (https://reactjs.org/) - Javascript framework
* [Semantic UI] (https://semantic-ui.com/) - CSS framework
* [Axios] (https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js