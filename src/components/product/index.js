import React, {Component} from 'react';
import './index.css';

export default class Product extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cubicWeight: null
        }
    }

    componentDidMount() {
        if (this.props.data.category == "Air Conditioners") {
            this.calculateCubicWeight();
        }
    }

    render() {
        return (
            <div className="four wide column">
                <div className="ui card">
                    <div className="content">
                        <div className="header">{this.props.data.title}</div>
                    </div>

                    <div className="content">
                        <div className="description">
                            <p>
                                {this.props.data.category}
                            </p>
                        </div>
                    </div>

                    <div className="extra content">
                        <div className="extra content">
                            <div className="ui list">
                                <div className="item">
                                    <div className="content">
                                        <strong>Weight:</strong> {this.props.data.weight} grams
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="content">
                                        {this.props.data.size && <span><strong>Width: </strong>{this.props.data.size.width}</span>}<p></p>
                                        {this.props.data.size && <span><strong>Length: </strong>{this.props.data.size.length}</span>}<p></p>
                                        {this.props.data.size && <span><strong>Height: </strong>{this.props.data.size.height}</span>}<p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {this.renderCubicWeight()}
                </div>
            </div>
        );
    }

    renderCubicWeight() {
        if (this.props.data.category == "Air Conditioners") {
            return (
                <div className="extra content">
                    <strong>Average Cubic Weight:</strong> {this.state.cubicWeight} kg
                </div>
            )
        }
    }

    calculateCubicWeight() {
        const {data} = this.props;

        // Convert each measurement to metres and use equation as required - returning 2 dec points
        let weight = ((data.size.length / 100 * data.size.height / 100 * data.size.width / 100) * 250).toFixed(2);

        this.setState({cubicWeight: weight});
    }
}