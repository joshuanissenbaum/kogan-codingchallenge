// [Kogan Coding Challenge]
//
// Joshua Nissenbaum

// Import React
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// Import Components
import Product from './components/product';

// Import Semantic
import './semantic/dist/semantic.css';

// Import packages
import axios from 'axios';

// Import Constants
import {apiURL} from './utils/constants';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            page: '/api/products/1',
            loading: true
        }

        this.loadData();
        this.handleScroll = this.handleScroll.bind(this);
    }
    
    render() {
        return (
            <div className="ui container" id="container">
                <h1 style={{marginTop: '2em'}}>Products</h1>

                <div className="ui stackable grid container" style={{marginTop: '2em'}}>
                    {this.renderProducts()}
                </div>

                {this.renderLoading()}
            </div>
        );
    }

    handleScroll() {
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight; // Height of the whole document
        const body = document.body; // Reference to the document body
        const html = document.documentElement; // Reference to the document
        const documentHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight); // Calculate the bottom of the window
        const windowBottom = windowHeight + window.pageYOffset;

        // The window is greater than or equal to the document height - user has reached the bottom
        if (windowBottom >= documentHeight) {
            if (!this.state.loading && this.state.page != null) {
                this.setState({loading: true});
                this.loadMoreData();
            }
        }
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll); // An event listener each time the user scrolls the DOM/window
    }

    // Initial request to the API end point
    loadData() {
        axios.get(apiURL + this.state.page).then((response) => {
            // Set the next page to be loaded, and the products array on component state
            this.setState({
                products: response.data.objects,
                page: response.data.next,
                loading: false
            });
        })
        .catch((error) => {
            alert(error);
            console.log(error);
        });
    }

    // Load extra data as the user scrolls down the page (infinite scroll)
    loadMoreData() {
        axios.get(apiURL + this.state.page).then((response) => {
            let products = this.state.products;

            // Push each object in the new pagination response into our current products array
            response.data.objects.map((object) => {
                products.push(object);
            });

            // Set the next page to be loaded, and the new products array on component state
            this.setState({
                products: products,
                page: response.data.next,
                loading: false
            });
        }).catch((error) => {
            alert(error);
            console.log(error);
        });
    }

    // Render the products data passing each object to the Product component as a prop
    renderProducts() {
        return this.state.products.map((product, index) => {
            return (
                <Product data={product} key={index}/>
            )
        })
    }

    // Render the loading spinner when the component state is fetching from the API
    renderLoading() {
        if (this.state.loading) {
            return (
                <div className="ui active centered inline loader"></div>
            );
        }
    }
}

// Initialise the root application
ReactDOM.render(
    <App/>,
    document.getElementById('root')
);
